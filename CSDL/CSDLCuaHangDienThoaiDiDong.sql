USE [DB_MobileStore_v2]
GO
/****** Object:  Table [dbo].[CT_HDBAN]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CT_HDBAN](
	[MaHDB] [int] NOT NULL,
	[MaSP] [int] NOT NULL,
	[SoLuongBan] [int] NULL,
	[DonGiaBan] [float] NULL,
	[TongTien] [float] NULL,
 CONSTRAINT [PK_CT_HDBAN] PRIMARY KEY CLUSTERED 
(
	[MaHDB] ASC,
	[MaSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CT_HDNHAP]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CT_HDNHAP](
	[MaSP] [int] NOT NULL,
	[MaHDN] [int] NOT NULL,
	[SoLuongNhap] [int] NULL,
	[DonGiaNhap] [float] NULL,
	[TongTien] [float] NULL,
 CONSTRAINT [PK_CT_HDNHAP] PRIMARY KEY CLUSTERED 
(
	[MaSP] ASC,
	[MaHDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DM_ManHinh]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DM_ManHinh](
	[MaManHinh] [char](10) NOT NULL,
	[TenManHinh] [nvarchar](50) NULL,
 CONSTRAINT [PK_DM_ManHinh] PRIMARY KEY CLUSTERED 
(
	[MaManHinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HOADONBAN]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HOADONBAN](
	[MaHDB] [int] IDENTITY(1,1) NOT NULL,
	[MaNV] [int] NULL,
	[MaKH] [int] NULL,
	[NgayLapHDB] [date] NULL,
	[ThanhTienHDB] [float] NULL,
	[GhiChu] [nvarchar](max) NULL,
 CONSTRAINT [PK_HOADONBAN] PRIMARY KEY CLUSTERED 
(
	[MaHDB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HOADONNHAP]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HOADONNHAP](
	[MaHDN] [int] IDENTITY(1,1) NOT NULL,
	[MaNCC] [int] NULL,
	[MaNV] [int] NULL,
	[NgayLapHDN] [date] NULL,
	[ThanhTienHDN] [float] NULL,
	[GhiChu] [nvarchar](max) NULL,
 CONSTRAINT [PK_HOADONNHAP] PRIMARY KEY CLUSTERED 
(
	[MaHDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KHACHHANG]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KHACHHANG](
	[MaKH] [int] IDENTITY(1,1) NOT NULL,
	[TenKH] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](250) NULL,
	[SdtKH] [char](11) NULL,
 CONSTRAINT [PK_KHACHHANG] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHACUNGCAP]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHACUNGCAP](
	[MaNCC] [int] IDENTITY(1,1) NOT NULL,
	[TenNCC] [nvarchar](50) NULL,
	[Email] [varchar](50) NULL,
	[SdtNCC] [char](11) NULL,
	[DiaChi] [nvarchar](50) NULL,
 CONSTRAINT [PK_NHACUNGCAP] PRIMARY KEY CLUSTERED 
(
	[MaNCC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NHANVIEN]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHANVIEN](
	[MaNV] [int] IDENTITY(1,1) NOT NULL,
	[TenNV] [nvarchar](50) NULL,
	[DiaChiNV] [nvarchar](250) NULL,
	[NgaySinh] [char](10) NULL,
	[SdtNV] [char](11) NULL,
	[TenDangNhap] [char](20) NULL,
 CONSTRAINT [PK_NHANVIEN] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhanQuyen]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhanQuyen](
	[MaNhomNguoiDung] [char](10) NOT NULL,
	[MaManHinh] [char](10) NOT NULL,
	[CoQuyen] [char](10) NULL,
 CONSTRAINT [PK_PhanQuyen] PRIMARY KEY CLUSTERED 
(
	[MaNhomNguoiDung] ASC,
	[MaManHinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QL_NGUOIDUNG]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QL_NGUOIDUNG](
	[TenDangNhap] [char](20) NOT NULL,
	[MatKhau] [varchar](50) NULL,
	[HoatDong] [bit] NULL,
 CONSTRAINT [PK_QL_NGUOIDUNG] PRIMARY KEY CLUSTERED 
(
	[TenDangNhap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QL_NguoiDungNhomNguoiDung]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QL_NguoiDungNhomNguoiDung](
	[TenDangNhap] [char](20) NOT NULL,
	[MaNhomNguoiDung] [char](10) NOT NULL,
	[GhiChu] [nvarchar](50) NULL,
 CONSTRAINT [PK_QL_NguoiDungNhomNguoiDung] PRIMARY KEY CLUSTERED 
(
	[TenDangNhap] ASC,
	[MaNhomNguoiDung] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QL_NhomNguoiDung]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QL_NhomNguoiDung](
	[MaNhom] [char](10) NOT NULL,
	[TenNhom] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
 CONSTRAINT [PK_QL_NhomNguoiDung] PRIMARY KEY CLUSTERED 
(
	[MaNhom] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SANPHAM]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SANPHAM](
	[MaSP] [int] IDENTITY(1,1) NOT NULL,
	[TenSP] [nvarchar](250) NULL,
	[DonGiaNhap] [float] NULL,
	[DonGiaBan] [float] NULL,
	[SoLuongTon] [int] NULL,
	[MoTa] [nvarchar](max) NULL,
	[MaThuongHieu] [int] NULL,
 CONSTRAINT [PK_SANPHAM] PRIMARY KEY CLUSTERED 
(
	[MaSP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[THUONGHIEU]    Script Date: 15/12/2019 9:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[THUONGHIEU](
	[MaThuongHieu] [int] IDENTITY(1,1) NOT NULL,
	[TenThuongHieu] [nvarchar](100) NULL,
 CONSTRAINT [PK_THUONGHIEU] PRIMARY KEY CLUSTERED 
(
	[MaThuongHieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[CT_HDBAN] ([MaHDB], [MaSP], [SoLuongBan], [DonGiaBan], [TongTien]) VALUES (134, 4, 2, 12000, 24000)
INSERT [dbo].[CT_HDBAN] ([MaHDB], [MaSP], [SoLuongBan], [DonGiaBan], [TongTien]) VALUES (134, 8, 1, 130000, 130000)
INSERT [dbo].[CT_HDBAN] ([MaHDB], [MaSP], [SoLuongBan], [DonGiaBan], [TongTien]) VALUES (135, 8, 50, 130000, 6500000)
INSERT [dbo].[CT_HDNHAP] ([MaSP], [MaHDN], [SoLuongNhap], [DonGiaNhap], [TongTien]) VALUES (8, 28, 50, 120000, 6000000)
INSERT [dbo].[CT_HDNHAP] ([MaSP], [MaHDN], [SoLuongNhap], [DonGiaNhap], [TongTien]) VALUES (9, 27, 21, 2000, 40000)
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'AddNhom   ', N'Thêm ND Vào Nhóm')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'BanHang   ', N'Bán Hàng')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'KhachHang ', N'Khách Hàng')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'MHNhap    ', N'Nhập Hàng')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'MHNV      ', N'Nhân Viên')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'NhaCC     ', N'Nhà Cung Cấp')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'NhanVien  ', N'Nhân Viên')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'NhapHang  ', N'Nhập Hàng')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'PhanQuyen ', N'Phân Quyền')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'SanPham   ', N'Sản Phẩm')
INSERT [dbo].[DM_ManHinh] ([MaManHinh], [TenManHinh]) VALUES (N'ThemNDNhom', N'Thêm Người Dùng Vào Nhóm')
SET IDENTITY_INSERT [dbo].[HOADONBAN] ON 

INSERT [dbo].[HOADONBAN] ([MaHDB], [MaNV], [MaKH], [NgayLapHDB], [ThanhTienHDB], [GhiChu]) VALUES (134, 1, 2, CAST(0x7F400B00 AS Date), 154000, N'')
INSERT [dbo].[HOADONBAN] ([MaHDB], [MaNV], [MaKH], [NgayLapHDB], [ThanhTienHDB], [GhiChu]) VALUES (135, 1, 1, CAST(0x7F400B00 AS Date), 6500000, N'')
INSERT [dbo].[HOADONBAN] ([MaHDB], [MaNV], [MaKH], [NgayLapHDB], [ThanhTienHDB], [GhiChu]) VALUES (136, 1, 1, CAST(0x7F400B00 AS Date), 0, N'')
INSERT [dbo].[HOADONBAN] ([MaHDB], [MaNV], [MaKH], [NgayLapHDB], [ThanhTienHDB], [GhiChu]) VALUES (137, 1, 1, CAST(0x7F400B00 AS Date), 0, N'')
SET IDENTITY_INSERT [dbo].[HOADONBAN] OFF
SET IDENTITY_INSERT [dbo].[HOADONNHAP] ON 

INSERT [dbo].[HOADONNHAP] ([MaHDN], [MaNCC], [MaNV], [NgayLapHDN], [ThanhTienHDN], [GhiChu]) VALUES (27, 1, 1, CAST(0x7F400B00 AS Date), 40000, N'')
INSERT [dbo].[HOADONNHAP] ([MaHDN], [MaNCC], [MaNV], [NgayLapHDN], [ThanhTienHDN], [GhiChu]) VALUES (28, 1, 1, CAST(0x7F400B00 AS Date), 6000000, N'')
SET IDENTITY_INSERT [dbo].[HOADONNHAP] OFF
SET IDENTITY_INSERT [dbo].[KHACHHANG] ON 

INSERT [dbo].[KHACHHANG] ([MaKH], [TenKH], [DiaChi], [SdtKH]) VALUES (1, N'Son Tung', N'Ho Chi Minh', N'0989999888 ')
INSERT [dbo].[KHACHHANG] ([MaKH], [TenKH], [DiaChi], [SdtKH]) VALUES (2, N'Chau Khai Phong', N'Dong Nai', N'0945454556 ')
INSERT [dbo].[KHACHHANG] ([MaKH], [TenKH], [DiaChi], [SdtKH]) VALUES (3, N'Ho Quang Hieu', N'Binh Duong', N'0934567899 ')
INSERT [dbo].[KHACHHANG] ([MaKH], [TenKH], [DiaChi], [SdtKH]) VALUES (4, N'Khac Viet', N'Hai Phong', N'0912345678 ')
SET IDENTITY_INSERT [dbo].[KHACHHANG] OFF
SET IDENTITY_INSERT [dbo].[NHACUNGCAP] ON 

INSERT [dbo].[NHACUNGCAP] ([MaNCC], [TenNCC], [Email], [SdtNCC], [DiaChi]) VALUES (1, N'Thế Giới Di Động', N'thegioididong@gmail.com', N'09865656566', N'14 Trường Chinh, P 12, Q Tân Bình, Tp HCM')
INSERT [dbo].[NHACUNGCAP] ([MaNCC], [TenNCC], [Email], [SdtNCC], [DiaChi]) VALUES (3, N'Vien Thong @', N'Vienthong@gamil.com', N'09865233665', N'19 Cong Hoa, P12, Q Tan Binh, Tp HCM')
INSERT [dbo].[NHACUNGCAP] ([MaNCC], [TenNCC], [Email], [SdtNCC], [DiaChi]) VALUES (4, N'FPT Shop', N'Fpt@gmial.com', N'0152333333 ', N'67, Tran Phu, Q 5 , Tp HCM')
INSERT [dbo].[NHACUNGCAP] ([MaNCC], [TenNCC], [Email], [SdtNCC], [DiaChi]) VALUES (5, N'Phong Vũ', N'PhongVu@gmail.com', N'0989336565 ', N'450, Cộng Hòa, P 14, Q Tân Bình, Tp HCM')
SET IDENTITY_INSERT [dbo].[NHACUNGCAP] OFF
SET IDENTITY_INSERT [dbo].[NHANVIEN] ON 

INSERT [dbo].[NHANVIEN] ([MaNV], [TenNV], [DiaChiNV], [NgaySinh], [SdtNV], [TenDangNhap]) VALUES (1, N'Lam Thien Lap', N'Ca Mau', N'1-1-1988  ', N'0945789789 ', N'a                   ')
INSERT [dbo].[NHANVIEN] ([MaNV], [TenNV], [DiaChiNV], [NgaySinh], [SdtNV], [TenDangNhap]) VALUES (2, N'Nguyen Thi Xuan', N'Ho Chi Minh', N'2-3-1994  ', N'0944778855 ', N'a                   ')
INSERT [dbo].[NHANVIEN] ([MaNV], [TenNV], [DiaChiNV], [NgaySinh], [SdtNV], [TenDangNhap]) VALUES (3, N'Do Hoang Thien', N'Dong Nai', N'02/02/1997', N'0987777454 ', N'a                   ')
INSERT [dbo].[NHANVIEN] ([MaNV], [TenNV], [DiaChiNV], [NgaySinh], [SdtNV], [TenDangNhap]) VALUES (4, N'Trần Minh Tuyến', N'Long An', N'02/01/1998', N'09833366699', N'a                   ')
SET IDENTITY_INSERT [dbo].[NHANVIEN] OFF
INSERT [dbo].[PhanQuyen] ([MaNhomNguoiDung], [MaManHinh], [CoQuyen]) VALUES (N'3         ', N'BanHang   ', N'False     ')
INSERT [dbo].[PhanQuyen] ([MaNhomNguoiDung], [MaManHinh], [CoQuyen]) VALUES (N'3         ', N'NhaCC     ', N'False     ')
INSERT [dbo].[PhanQuyen] ([MaNhomNguoiDung], [MaManHinh], [CoQuyen]) VALUES (N'4         ', N'KhachHang ', N'False     ')
INSERT [dbo].[QL_NGUOIDUNG] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'a                   ', N'a', 1)
INSERT [dbo].[QL_NGUOIDUNG] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'nv1                 ', N'a', 1)
INSERT [dbo].[QL_NGUOIDUNG] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'nv2                 ', N'a', 0)
INSERT [dbo].[QL_NGUOIDUNG] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'q                   ', N'1', 1)
INSERT [dbo].[QL_NGUOIDUNG] ([TenDangNhap], [MatKhau], [HoatDong]) VALUES (N'TranTuyen           ', N'123', 1)
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'a                   ', N'1         ', NULL)
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'q                   ', N'1         ', NULL)
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'TranTuyen           ', N'2         ', NULL)
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'TranTuyen           ', N'3         ', NULL)
INSERT [dbo].[QL_NguoiDungNhomNguoiDung] ([TenDangNhap], [MaNhomNguoiDung], [GhiChu]) VALUES (N'TranTuyen           ', N'4         ', NULL)
INSERT [dbo].[QL_NhomNguoiDung] ([MaNhom], [TenNhom], [GhiChu]) VALUES (N'1         ', N'Quản Lý', NULL)
INSERT [dbo].[QL_NhomNguoiDung] ([MaNhom], [TenNhom], [GhiChu]) VALUES (N'2         ', N'Nhân Viên Bán Hàng', NULL)
INSERT [dbo].[QL_NhomNguoiDung] ([MaNhom], [TenNhom], [GhiChu]) VALUES (N'3         ', N'Nhân Viên Nhập Hàng', NULL)
INSERT [dbo].[QL_NhomNguoiDung] ([MaNhom], [TenNhom], [GhiChu]) VALUES (N'4         ', N'Nhân Viên Kho', NULL)
SET IDENTITY_INSERT [dbo].[SANPHAM] ON 

INSERT [dbo].[SANPHAM] ([MaSP], [TenSP], [DonGiaNhap], [DonGiaBan], [SoLuongTon], [MoTa], [MaThuongHieu]) VALUES (1, N'Samsung S8', 7000, 9000, 0, N'SamSung GaLaXy S8 64GB', 1)
INSERT [dbo].[SANPHAM] ([MaSP], [TenSP], [DonGiaNhap], [DonGiaBan], [SoLuongTon], [MoTa], [MaThuongHieu]) VALUES (2, N'iPhone 8s', 8800, 11000, 0, N'Iphone 8', 2)
INSERT [dbo].[SANPHAM] ([MaSP], [TenSP], [DonGiaNhap], [DonGiaBan], [SoLuongTon], [MoTa], [MaThuongHieu]) VALUES (3, N'HTC One Me', 4000, 5800, 45, N'HTC One Me', 5)
INSERT [dbo].[SANPHAM] ([MaSP], [TenSP], [DonGiaNhap], [DonGiaBan], [SoLuongTon], [MoTa], [MaThuongHieu]) VALUES (4, N'Sony Xperia 1', 11000, 12000, 10, N'Sony Xperia 1 512GB', 1)
INSERT [dbo].[SANPHAM] ([MaSP], [TenSP], [DonGiaNhap], [DonGiaBan], [SoLuongTon], [MoTa], [MaThuongHieu]) VALUES (7, N'SamSung GaLaXy S9', 42000, 52000, 147, N'SamSung GaLaXy S9 128GB', 1)
INSERT [dbo].[SANPHAM] ([MaSP], [TenSP], [DonGiaNhap], [DonGiaBan], [SoLuongTon], [MoTa], [MaThuongHieu]) VALUES (8, N'Iphone X 512GB', 120000, 130000, 50, N'Iphone X 512GB ', 2)
INSERT [dbo].[SANPHAM] ([MaSP], [TenSP], [DonGiaNhap], [DonGiaBan], [SoLuongTon], [MoTa], [MaThuongHieu]) VALUES (9, N'Oppo Find 5', 2000, 2500, 100, N'Điện thoại Oppo Find 5 ', 4)
SET IDENTITY_INSERT [dbo].[SANPHAM] OFF
SET IDENTITY_INSERT [dbo].[THUONGHIEU] ON 

INSERT [dbo].[THUONGHIEU] ([MaThuongHieu], [TenThuongHieu]) VALUES (1, N'Samsung')
INSERT [dbo].[THUONGHIEU] ([MaThuongHieu], [TenThuongHieu]) VALUES (2, N'iPhone')
INSERT [dbo].[THUONGHIEU] ([MaThuongHieu], [TenThuongHieu]) VALUES (3, N'Sony')
INSERT [dbo].[THUONGHIEU] ([MaThuongHieu], [TenThuongHieu]) VALUES (4, N'Oppo')
INSERT [dbo].[THUONGHIEU] ([MaThuongHieu], [TenThuongHieu]) VALUES (5, N'HTC')
INSERT [dbo].[THUONGHIEU] ([MaThuongHieu], [TenThuongHieu]) VALUES (6, N'Vivo')
SET IDENTITY_INSERT [dbo].[THUONGHIEU] OFF
ALTER TABLE [dbo].[CT_HDBAN]  WITH CHECK ADD  CONSTRAINT [FK_CT_HDBAN_HOADONBAN] FOREIGN KEY([MaHDB])
REFERENCES [dbo].[HOADONBAN] ([MaHDB])
GO
ALTER TABLE [dbo].[CT_HDBAN] CHECK CONSTRAINT [FK_CT_HDBAN_HOADONBAN]
GO
ALTER TABLE [dbo].[CT_HDBAN]  WITH CHECK ADD  CONSTRAINT [FK_CT_HDBAN_SANPHAM] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SANPHAM] ([MaSP])
GO
ALTER TABLE [dbo].[CT_HDBAN] CHECK CONSTRAINT [FK_CT_HDBAN_SANPHAM]
GO
ALTER TABLE [dbo].[CT_HDNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CT_HDNHAP_HOADONNHAP] FOREIGN KEY([MaHDN])
REFERENCES [dbo].[HOADONNHAP] ([MaHDN])
GO
ALTER TABLE [dbo].[CT_HDNHAP] CHECK CONSTRAINT [FK_CT_HDNHAP_HOADONNHAP]
GO
ALTER TABLE [dbo].[CT_HDNHAP]  WITH CHECK ADD  CONSTRAINT [FK_CT_HDNHAP_SANPHAM] FOREIGN KEY([MaSP])
REFERENCES [dbo].[SANPHAM] ([MaSP])
GO
ALTER TABLE [dbo].[CT_HDNHAP] CHECK CONSTRAINT [FK_CT_HDNHAP_SANPHAM]
GO
ALTER TABLE [dbo].[HOADONBAN]  WITH CHECK ADD  CONSTRAINT [FK_HOADONBAN_KHACHHANG] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KHACHHANG] ([MaKH])
GO
ALTER TABLE [dbo].[HOADONBAN] CHECK CONSTRAINT [FK_HOADONBAN_KHACHHANG]
GO
ALTER TABLE [dbo].[HOADONBAN]  WITH CHECK ADD  CONSTRAINT [FK_HOADONBAN_NHANVIEN] FOREIGN KEY([MaNV])
REFERENCES [dbo].[NHANVIEN] ([MaNV])
GO
ALTER TABLE [dbo].[HOADONBAN] CHECK CONSTRAINT [FK_HOADONBAN_NHANVIEN]
GO
ALTER TABLE [dbo].[HOADONNHAP]  WITH CHECK ADD  CONSTRAINT [FK_HOADONNHAP_NHACUNGCAP] FOREIGN KEY([MaNCC])
REFERENCES [dbo].[NHACUNGCAP] ([MaNCC])
GO
ALTER TABLE [dbo].[HOADONNHAP] CHECK CONSTRAINT [FK_HOADONNHAP_NHACUNGCAP]
GO
ALTER TABLE [dbo].[HOADONNHAP]  WITH CHECK ADD  CONSTRAINT [FK_HOADONNHAP_NHANVIEN] FOREIGN KEY([MaNV])
REFERENCES [dbo].[NHANVIEN] ([MaNV])
GO
ALTER TABLE [dbo].[HOADONNHAP] CHECK CONSTRAINT [FK_HOADONNHAP_NHANVIEN]
GO
ALTER TABLE [dbo].[NHANVIEN]  WITH CHECK ADD  CONSTRAINT [FK_NHANVIEN_QL_NGUOIDUNG] FOREIGN KEY([TenDangNhap])
REFERENCES [dbo].[QL_NGUOIDUNG] ([TenDangNhap])
GO
ALTER TABLE [dbo].[NHANVIEN] CHECK CONSTRAINT [FK_NHANVIEN_QL_NGUOIDUNG]
GO
ALTER TABLE [dbo].[QL_NguoiDungNhomNguoiDung]  WITH CHECK ADD  CONSTRAINT [FK_QL_NguoiDungNhomNguoiDung_QL_NGUOIDUNG] FOREIGN KEY([TenDangNhap])
REFERENCES [dbo].[QL_NGUOIDUNG] ([TenDangNhap])
GO
ALTER TABLE [dbo].[QL_NguoiDungNhomNguoiDung] CHECK CONSTRAINT [FK_QL_NguoiDungNhomNguoiDung_QL_NGUOIDUNG]
GO
ALTER TABLE [dbo].[SANPHAM]  WITH CHECK ADD  CONSTRAINT [FK_SANPHAM_THUONGHIEU] FOREIGN KEY([MaThuongHieu])
REFERENCES [dbo].[THUONGHIEU] ([MaThuongHieu])
GO
ALTER TABLE [dbo].[SANPHAM] CHECK CONSTRAINT [FK_SANPHAM_THUONGHIEU]
GO
