﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;

namespace PM_PhoneStore_V2
{
    public partial class frmThongKeDoanhThu : DevExpress.XtraEditors.XtraForm
    {
        ThongKeDoanhThuDAL Dal = new ThongKeDoanhThuDAL();
        public frmThongKeDoanhThu()
        {
            InitializeComponent();
        }
        //public static string sql;
        private void frmThongKeDoanhThu_Load(object sender, EventArgs e)
        {
           


        }

        private void cbNhanVien_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void cbNhanVien_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }

        private void cbNhanVien_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            
            ReportDoanhThu rpt = new ReportDoanhThu();
            crystalReportViewer1.ReportSource = rpt;
            crystalReportViewer1.Refresh();
            rpt.SetParameterValue("NgayLapHD", dateHoaDon.Value.ToString("yyyy-dd-mm"));
            rpt.SetDatabaseLogon("sa", "sa2012", "ADMIN\\SQLEXPRESS", "DB_MobileStore_v2");
        }
    }
}