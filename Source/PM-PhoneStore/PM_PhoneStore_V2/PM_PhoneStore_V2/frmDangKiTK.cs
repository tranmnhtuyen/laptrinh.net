﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;

namespace PM_PhoneStore_V2
{
    public partial class frmDangKiTK : DevExpress.XtraEditors.XtraForm
    {
        DKTaiKhoanDAL Dal = new DKTaiKhoanDAL();
        public frmDangKiTK()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //Kiem tra rong
            if (string.IsNullOrEmpty(txtTenDangNhap.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống tên đăng nhập! ");
                this.txtTenDangNhap.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtMatKhau.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống mật khẩu ! ");
                this.txtMatKhau.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtNhapLaiMK.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ nhập lại mật khẩu! ");
                this.txtNhapLaiMK.Focus();
                return;
            }

            if (txtMatKhau.Text != txtNhapLaiMK.Text)
            {
                MessageBox.Show("Nhập lại mât khẩu mới không khớp!");
            }
            else 
            {
                QL_NGUOIDUNG nd = new QL_NGUOIDUNG();
                nd.TenDangNhap = txtTenDangNhap.Text;
                nd.MatKhau = txtMatKhau.Text;
                nd.HoatDong = Convert.ToBoolean(1);
                if (Dal.LuuThongTinTaiKhoan(nd) == 1)
                {
                    MessageBox.Show("Lưu thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Lưu thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }

        }

        private void frmDangKiTK_Load(object sender, EventArgs e)
        {
            
        }
    }
}