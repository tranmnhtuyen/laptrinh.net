﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;

namespace PM_PhoneStore_V2
{
    public partial class frmKhachHang : DevExpress.XtraEditors.XtraForm
    {
        KhachHangDAL Dal = new KhachHangDAL();
        public frmKhachHang()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmKhachHang_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Dal.LoadDataGridKH();

            txtMaKH.Enabled = false;
            txtTenKH.Focus();

            txtTenKH.Enabled = false;
            txtDiaChi.Enabled = false;
            txtSDT.Enabled = false;
            
            btnLuu.Enabled = false;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;
            btnHuy.Enabled = false;
        }

        private void bntThem_Click(object sender, EventArgs e)
        {
            txtTenKH.Enabled = true;
            txtTenKH.Focus();
            txtDiaChi.Enabled = true;
            txtSDT.Enabled = true;
            
            btnLuu.Enabled = true;
            btnHuy.Enabled = true;

            txtMaKH.Text = "";
            txtTenKH.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            //Kiem tra rong
            if (string.IsNullOrEmpty(txtTenKH.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống tên khách hàng! ");
                this.txtTenKH.Focus();
                return;
            }
            

            KHACHHANG kh = new KHACHHANG();
            //kh.MaKH = Convert.ToInt32(txtMaKH.Text);
            kh.TenKH = txtTenKH.Text;
            kh.SdtKH = txtSDT.Text;
            kh.DiaChi = txtDiaChi.Text;
            if (Dal.LuuThongTinKhachHang(kh) == 1)
            {
                MessageBox.Show("Lưu thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("Lưu thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            dataGridView1.DataSource = Dal.LoadDataGridKH();


            txtMaKH.Text = "";
            txtTenKH.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            //Kiem tra rong
           

            KHACHHANG kh = new KHACHHANG();
            kh.MaKH = Convert.ToInt32(txtMaKH.Text);
            kh.TenKH = txtTenKH.Text;
            kh.SdtKH = txtSDT.Text;
            kh.DiaChi = txtDiaChi.Text;
            if (Dal.SuaThongTinKhachHang(kh.MaKH, kh.TenKH, kh.DiaChi , kh.SdtKH) == 1)
            {
                MessageBox.Show("Đã sửa thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("Sửa thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            dataGridView1.DataSource = Dal.LoadDataGridKH();
            txtMaKH.Text = "";
            txtTenKH.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                KHACHHANG kh = new KHACHHANG();
                kh.MaKH = Convert.ToInt32(txtMaKH.Text);
                if (Dal.XoaThongTinKhachHang(kh.MaKH) == 1)
                {
                    MessageBox.Show("Đã xóa thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Xóa thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                dataGridView1.DataSource = Dal.LoadDataGridKH();
            }
            catch
            {
                MessageBox.Show("Bạn không thể xóa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            txtMaKH.Text = "";
            txtTenKH.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            txtMaKH.Enabled = false;
            txtTenKH.Focus();

            txtTenKH.Enabled = false;
            txtDiaChi.Enabled = false;
            txtSDT.Enabled = false;
            
            btnLuu.Enabled = false;
            btnSua.Enabled = false;
            btnXoa.Enabled = false;

            txtMaKH.Text = "";
            txtTenKH.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int numrow;
            numrow = e.RowIndex;
            txtMaKH.Text = dataGridView1.Rows[numrow].Cells[0].Value.ToString();
            txtTenKH.Text = dataGridView1.Rows[numrow].Cells[1].Value.ToString();
            txtDiaChi.Text = dataGridView1.Rows[numrow].Cells[2].Value.ToString();
            txtSDT.Text = dataGridView1.Rows[numrow].Cells[3].Value.ToString();

            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnHuy.Enabled = true;

            txtTenKH.Enabled = true;
            txtTenKH.Focus();
            txtDiaChi.Enabled = true;
            txtSDT.Enabled = true;
        }
    }
}