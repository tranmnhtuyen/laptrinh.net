﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace PM_PhoneStore_V2
{
    public partial class frmThemNguoiDungVaoNhom : DevExpress.XtraEditors.XtraForm
    {
        public frmThemNguoiDungVaoNhom()
        {
            InitializeComponent();
        }

        private void qL_NGUOIDUNGBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.qL_NGUOIDUNGBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.nguoiDung);

        }

        private void qL_NGUOIDUNGBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.qL_NGUOIDUNGBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.nguoiDung);

        }

        private void frmThemNguoiDungVaoNhom_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'nguoiDung.QL_NhomNguoiDung' table. You can move, or remove it, as needed.
            this.qL_NhomNguoiDungTableAdapter.Fill(this.nguoiDung.QL_NhomNguoiDung);
            // TODO: This line of code loads data into the 'nguoiDung.QL_NGUOIDUNG' table. You can move, or remove it, as needed.
            this.qL_NGUOIDUNGTableAdapter.Fill(this.nguoiDung.QL_NGUOIDUNG);

        }

        private void fill_DKToolStripButton_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    this.qL_NguoiDungNhomNguoiDung1TableAdapter.Fill_DK(this.nguoiDung.QL_NguoiDungNhomNguoiDung1, maNhomNguoiDungToolStripTextBox.Text);
            //}
            //catch (System.Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.Message);
            //}

        }

        private void qL_NhomNguoiDungComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //qL_NhomNguoiDungComboBox
            try
            {
                this.qL_NguoiDungNhomNguoiDung1TableAdapter.Fill_DK(this.nguoiDung.QL_NguoiDungNhomNguoiDung1, qL_NhomNguoiDungComboBox.SelectedValue.ToString());
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
    }
}