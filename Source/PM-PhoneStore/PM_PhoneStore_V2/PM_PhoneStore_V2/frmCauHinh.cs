﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace PM_PhoneStore_V2
{
    public partial class frmCauHinh : DevExpress.XtraEditors.XtraForm
    {
        QL_NguoiDung CauHinh = new QL_NguoiDung();
        public frmCauHinh()
        {
            InitializeComponent();
        }

        private void cbServer_DropDown(object sender, EventArgs e)
        {
            DataTable dt = CauHinh.GetServerName();
            cbServer.Items.Clear();
            foreach (System.Data.DataRow row in dt.Rows)
            {
                foreach (System.Data.DataColumn col in dt.Columns)
                {
                    cbServer.Items.Add(row[col]);
                }
            }
        }

        public void ChangeConnectionString(string pServerName, string pDataBase, string pUser,string pPass)
        {
            Properties.Settings.Default.LTWNCConn = "Data Source=" + pServerName + ";Initial Catalog=" + pDataBase + ";User ID=" + pUser + ";pwd = " + pPass + ""; Properties.Settings.Default.Save();
        }
        private bool CheckedBeforSearchNameDB()
        {
            if (cbServer.Text == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập server name", "Thông Báo");
                cbServer.Focus();
                return false;
            }
            if (txtUserName.Text == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập Username", "Thông Báo");
                txtUserName.Focus();
                return false;
            }
            if (txtPassWord.Text == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập Password", "Thông Báo");
                txtPassWord.Focus();
                return false;
            }
            return true;
        }
        private void cbDatabase_DropDown(object sender, EventArgs e)
        {
            if (CheckedBeforSearchNameDB())
            {
                cbDatabase.Items.Clear();
                List<string> _list = CauHinh.GetDatabaseName(cbServer.Text,txtUserName.Text, txtPassWord.Text);
                if (_list == null)
                {
                    MessageBox.Show("Thông tin cấu hình chưa chính xác");
                    return;
                }
                foreach (string item in _list)
                {
                    cbDatabase.Items.Add(item);
                }
            }
        }

        private void btnLuuLai_Click(object sender, EventArgs e)
        {
            ChangeConnectionString(cbServer.Text, cbDatabase.Text,txtUserName.Text, txtPassWord.Text);
            this.Close();
        }

    }
}