﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace PM_PhoneStore_V2
{
    public partial class frmHoaDonBanHang : DevExpress.XtraEditors.XtraForm
    {
        public frmHoaDonBanHang()
        {
            InitializeComponent();
        }

        private void frmHoaDonBanHang_Load(object sender, EventArgs e)
        {
            ReportHoaDonBanHang rpt = new ReportHoaDonBanHang();
            crystalReportViewer1.ReportSource = rpt;
            crystalReportViewer1.Refresh();
            //rpt.SetParameterValue("", );

            rpt.SetParameterValue("LocMaHDB", BanHang.MaHoaDon);
            rpt.SetDatabaseLogon("sa", "sa2012", "ADMIN\\SQLEXPRESS", "DB_MobileStore_v2");
            
        }
    }
}