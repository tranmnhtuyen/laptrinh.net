﻿namespace PM_PhoneStore_V2
{
    partial class frmTrangChu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTrangChu));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.PhanQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.NhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.NhapHang = new DevExpress.XtraBars.BarButtonItem();
            this.NhaCC = new DevExpress.XtraBars.BarButtonItem();
            this.BanHang = new DevExpress.XtraBars.BarButtonItem();
            this.KhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.MHNV = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.ThemNDNhom = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.C = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonControl1.SearchEditItem,
            this.barButtonItem1,
            this.PhanQuyen,
            this.barButtonItem3,
            this.NhanVien,
            this.barButtonItem5,
            this.NhapHang,
            this.NhaCC,
            this.BanHang,
            this.KhachHang,
            this.MHNV,
            this.barButtonItem11,
            this.barButtonItem2,
            this.ThemNDNhom,
            this.barButtonItem4,
            this.skinRibbonGalleryBarItem1});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 18;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.C,
            this.ribbonPage3});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.ribbonControl1.Size = new System.Drawing.Size(685, 143);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Đăng Xuất";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // PhanQuyen
            // 
            this.PhanQuyen.Caption = "Phân Quyền";
            this.PhanQuyen.Id = 2;
            this.PhanQuyen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("PhanQuyen.ImageOptions.Image")));
            this.PhanQuyen.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("PhanQuyen.ImageOptions.LargeImage")));
            this.PhanQuyen.Name = "PhanQuyen";
            this.PhanQuyen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PhanQuyen_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // NhanVien
            // 
            this.NhanVien.Caption = "Người Dùng";
            this.NhanVien.Id = 4;
            this.NhanVien.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("NhanVien.ImageOptions.Image")));
            this.NhanVien.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("NhanVien.ImageOptions.LargeImage")));
            this.NhanVien.Name = "NhanVien";
            this.NhanVien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Phân Quyền";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // NhapHang
            // 
            this.NhapHang.Caption = "Nhập Hàng";
            this.NhapHang.Id = 6;
            this.NhapHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("NhapHang.ImageOptions.Image")));
            this.NhapHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("NhapHang.ImageOptions.LargeImage")));
            this.NhapHang.Name = "NhapHang";
            this.NhapHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.MHNhap_ItemClick);
            // 
            // NhaCC
            // 
            this.NhaCC.Caption = "Nhà Cung Cấp";
            this.NhaCC.Id = 8;
            this.NhaCC.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("NhaCC.ImageOptions.Image")));
            this.NhaCC.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("NhaCC.ImageOptions.LargeImage")));
            this.NhaCC.Name = "NhaCC";
            this.NhaCC.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.NhaCC_ItemClick);
            // 
            // BanHang
            // 
            this.BanHang.Caption = "Bán Hàng";
            this.BanHang.Id = 9;
            this.BanHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BanHang.ImageOptions.Image")));
            this.BanHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("BanHang.ImageOptions.LargeImage")));
            this.BanHang.Name = "BanHang";
            this.BanHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // KhachHang
            // 
            this.KhachHang.Caption = "Khách Hàng";
            this.KhachHang.Id = 10;
            this.KhachHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("KhachHang.ImageOptions.Image")));
            this.KhachHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("KhachHang.ImageOptions.LargeImage")));
            this.KhachHang.Name = "KhachHang";
            this.KhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.KhachHang_ItemClick);
            // 
            // MHNV
            // 
            this.MHNV.Caption = "Nhân Viên";
            this.MHNV.Id = 11;
            this.MHNV.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("MHNV.ImageOptions.Image")));
            this.MHNV.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("MHNV.ImageOptions.LargeImage")));
            this.MHNV.Name = "MHNV";
            this.MHNV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.MHNV_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "Phân Quyền";
            this.barButtonItem11.Id = 12;
            this.barButtonItem11.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem11.ImageOptions.Image")));
            this.barButtonItem11.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem11.ImageOptions.LargeImage")));
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Đổi Mật Khẩu";
            this.barButtonItem2.Id = 14;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.LargeImage")));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // ThemNDNhom
            // 
            this.ThemNDNhom.Caption = "Thêm Người Dùng Vào Nhóm";
            this.ThemNDNhom.Id = 15;
            this.ThemNDNhom.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ThemNDNhom.ImageOptions.Image")));
            this.ThemNDNhom.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("ThemNDNhom.ImageOptions.LargeImage")));
            this.ThemNDNhom.Name = "ThemNDNhom";
            this.ThemNDNhom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ThemNDNhom_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Sản Phẩm";
            this.barButtonItem4.Id = 16;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick_1);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Hệ Thống";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.PhanQuyen);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup1.ItemLinks.Add(this.ThemNDNhom);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            // 
            // C
            // 
            this.C.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3,
            this.ribbonPageGroup4});
            this.C.Name = "C";
            this.C.Text = "Chức Năng";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.NhapHang);
            this.ribbonPageGroup3.ItemLinks.Add(this.BanHang);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Nghiệp Vụ";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.NhaCC);
            this.ribbonPageGroup4.ItemLinks.Add(this.KhachHang);
            this.ribbonPageGroup4.ItemLinks.Add(this.MHNV);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Quản Lý";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Thống Kê";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "ribbonPageGroup5";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "ribbonPageGroup2";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 17;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // frmTrangChu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 434);
            this.Controls.Add(this.ribbonControl1);
            this.IsMdiContainer = true;
            this.Name = "frmTrangChu";
            this.Ribbon = this.ribbonControl1;
            this.Text = "Trang Chủ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTrangChu_FormClosing);
            this.Load += new System.EventHandler(this.frmTrangChu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem PhanQuyen;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem NhanVien;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.BarButtonItem NhapHang;
        private DevExpress.XtraBars.BarButtonItem NhaCC;
        private DevExpress.XtraBars.BarButtonItem BanHang;
        private DevExpress.XtraBars.BarButtonItem KhachHang;
        private DevExpress.XtraBars.BarButtonItem MHNV;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.Ribbon.RibbonPage C;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem ThemNDNhom;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
    }
}