﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;
namespace PM_PhoneStore_V2
{
    public partial class frnNhaCungCap : DevExpress.XtraEditors.XtraForm
    {
        NhaCungCapDAL NhaCungCapDal = new NhaCungCapDAL();
        public frnNhaCungCap()
        {
            InitializeComponent();
        }

        private void frnNhaCungCap_Load(object sender, EventArgs e)
        {
            txtMaNCC.Enabled = false;
            dataGridView1.DataSource = NhaCungCapDal.LoadDataGridNhaCC();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            //Kiểm tra rỗng
            if (string.IsNullOrEmpty(txtTenNCC.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống tên nhà cung cấp");
                this.txtTenNCC.Focus();
                return;
            }

            NHACUNGCAP ncc = new NHACUNGCAP();
            ncc.TenNCC = txtTenNCC.Text;
            ncc.Email = txtEmail.Text;
            ncc.SdtNCC = txtSDT.Text;
            ncc.DiaChi = txtDiaChi.Text;
            if (NhaCungCapDal.LuuTTNhaCC(ncc) == 1)
            {
                MessageBox.Show("Lưu thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("Luw thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            dataGridView1.DataSource = NhaCungCapDal.LoadDataGridNhaCC();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            NHACUNGCAP ncc = new NHACUNGCAP();
            ncc.MaNCC = Convert.ToInt32(txtMaNCC.Text);
            ncc.TenNCC = txtTenNCC.Text;
            ncc.Email = txtEmail.Text;
            ncc.SdtNCC = txtSDT.Text;
            ncc.DiaChi = txtDiaChi.Text;
            if (NhaCungCapDal.SuaNhaCC(ncc.MaNCC, ncc.TenNCC, ncc.Email, ncc.SdtNCC, ncc.DiaChi) == 1)
            {
                MessageBox.Show("Đã sửa thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("Sửa thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            dataGridView1.DataSource = NhaCungCapDal.LoadDataGridNhaCC();
            txtMaNCC.Text = "";
            txtTenNCC.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
            txtEmail.Text = "";
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int numrow;
            numrow = e.RowIndex;
            txtMaNCC.Text = dataGridView1.Rows[numrow].Cells[0].Value.ToString();
            txtTenNCC.Text = dataGridView1.Rows[numrow].Cells[1].Value.ToString();
            txtEmail.Text = dataGridView1.Rows[numrow].Cells[2].Value.ToString();
            txtSDT.Text = dataGridView1.Rows[numrow].Cells[3].Value.ToString();
            txtDiaChi.Text = dataGridView1.Rows[numrow].Cells[4].Value.ToString();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                NHACUNGCAP ncc = new NHACUNGCAP();
                ncc.MaNCC = Convert.ToInt32(txtMaNCC.Text);
                //int makho = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                if (NhaCungCapDal.XoaNhaCC(ncc.MaNCC) == 1)
                {
                    MessageBox.Show("Đã xóa thành công rồi!", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Xóa thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                dataGridView1.DataSource = NhaCungCapDal.LoadDataGridNhaCC();
            }
            catch
            {
                MessageBox.Show("Bạn không thể xóa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            txtMaNCC.Text = "";
            txtTenNCC.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
            txtEmail.Text = "";
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            txtMaNCC.Text = "";
            txtTenNCC.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
            txtEmail.Text = "";
        }
    }
}