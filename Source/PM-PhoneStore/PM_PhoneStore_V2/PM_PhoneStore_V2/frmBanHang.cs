﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;

namespace PM_PhoneStore_V2
{
    public partial class BanHang : DevExpress.XtraEditors.XtraForm
    {
        BanHangDAL BanHangDal = new BanHangDAL();
        public BanHang()
        {
            InitializeComponent();
        }
        public static int MaHoaDon;
        private void frmBanHang_Load(object sender, EventArgs e)
        {
            txtMaHD.Enabled = false;
            //cbNhanVien.Enabled = false;
            //groupThongTinSP.Enabled = false;
            cbSanPham.Enabled = false;
            txtSoLuongMua.Enabled = false;
            txtDonGiaBan.Enabled = false;
            txtMoTa.Enabled = false;
            txtSoLuongTon.Enabled = false;
            txtTongTien.Enabled = false;

            //button
            btnThanhToan.Enabled = false;
            btnInHD.Enabled = false;
            btnHuyHoaDon.Enabled = false;
            txtThanhTien.Enabled = false;
            btnInHD.Enabled = false;
            //xu ly
            int GiamGia;
            GiamGia = Convert.ToInt32(txtGiamGia.Text);


            cbKhachHang.DataSource = BanHangDal.LoadCbKhachHang();
            cbKhachHang.DisplayMember = "TenKH";
            cbKhachHang.ValueMember = "MaKH";

            cbSanPham.DataSource = BanHangDal.LoadCbSanPham();
            cbSanPham.DisplayMember = "TenSP";
            cbSanPham.ValueMember = "MaSP";

            cbNhanVien.DataSource = BanHangDal.LoadCbNhanVien();
            cbNhanVien.DisplayMember = "TenNV";
            cbNhanVien.ValueMember = "MaNV";
        }

        private void btnThemHD_Click(object sender, EventArgs e)
        {
            //groupThongTinSP.Enabled = true;
            cbSanPham.Enabled = true;
            txtSoLuongMua.Enabled = true;
            //button
            btnThanhToan.Enabled = true;
            //btnInHD.Enabled = true;
            btnHuyHoaDon.Enabled = true;

            //Xu ly
            //Kiem tra rong
            HOADONBAN hdn = new HOADONBAN();
            //hdn.MaHDB = Convert.ToInt32(txtMaHD.Text);
            hdn.MaNV = Convert.ToInt32(cbNhanVien.SelectedValue.ToString());
            hdn.MaKH = Convert.ToInt32(cbKhachHang.SelectedValue.ToString());
            hdn.NgayLapHDB = DateTime.Parse(dateNgayLapHD.Value.ToString());
            hdn.ThanhTienHDB = 0;
            hdn.GhiChu = txtGhiChu.Text;
            if (BanHangDal.LuuThongTinHoaDonBan(hdn) == 1)
            {
                MessageBox.Show("Lưu thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("Lưu thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            txtMaHD.Text = BanHangDal.LayMaHoaDon();
        }

        private void cbSanPham_TextChanged(object sender, EventArgs e)
        {
            BanHangDal.LayThongTinSanPham(cbSanPham.SelectedValue.ToString(), txtDonGiaBan, txtMoTa, txtSoLuongTon);

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            //Kiểm tra rỗng
            if (string.IsNullOrEmpty(txtSoLuongMua.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống tên sản phẩm! ");
                this.txtSoLuongMua.Focus();
                return;
            }
            if (Convert.ToInt32(txtSoLuongMua.Text)> Convert.ToInt32(txtSoLuongTon.Text))
            {
                MessageBox.Show("Hàng trong kho chỉ còn: " + txtSoLuongTon.Text + " sản phẩm.Không đủ đáp ứng số lượng của bạn");
            }
            else
            {
                int n = dataGridView1.Rows.Add();

                //dataGridView1.Rows[n].Cells[0].Value = Convert.ToInt32(cbSanPham.DisplayMember.ToString());
                dataGridView1.Rows[n].Cells[0].Value = cbSanPham.SelectedValue.ToString();
                dataGridView1.Rows[n].Cells[1].Value = cbSanPham.Text;

                dataGridView1.Rows[n].Cells[2].Value = Convert.ToInt32(txtSoLuongMua.Text);
                dataGridView1.Rows[n].Cells[3].Value = float.Parse(txtDonGiaBan.Text);
                dataGridView1.Rows[n].Cells[4].Value = (double.Parse(txtDonGiaBan.Text)) * (double.Parse(txtSoLuongMua.Text));
                dataGridView1.Rows[n].Cells[5].Value = (Convert.ToInt32(txtSoLuongTon.Text)) - Convert.ToInt32((txtSoLuongMua.Text));
                //dataGridView1.Rows[n].Cells[7].Value = float.Parse(txtThuongHieu.Text);

                txtDonGiaBan.Text = "";
                txtMoTa.Text = "";
                txtSoLuongMua.Text = "";
                txtSoLuongTon.Text = "";
                txtTongTien.Text = "";

                int sc = dataGridView1.Rows.Count;
                double thanhtien = 0;
                for (int j = 0; j < sc; j++)
                {
                    thanhtien += double.Parse(dataGridView1.Rows[j].Cells[4].Value.ToString());
                }
                txtThanhTien.Text = thanhtien.ToString();
            }
           
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            cbSanPham.Enabled = true;
            int rowSelected = dataGridView1.CurrentRow.Index;
            dataGridView1.Rows.Remove(dataGridView1.Rows[rowSelected]);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int numrow;
            numrow = e.RowIndex;
            cbSanPham.Text = dataGridView1.Rows[numrow].Cells[0].Value.ToString();
            txtDonGiaBan.Text = dataGridView1.Rows[numrow].Cells[3].Value.ToString();
            txtSoLuongMua.Text = dataGridView1.Rows[numrow].Cells[2].Value.ToString();
            txtTongTien.Text = dataGridView1.Rows[numrow].Cells[4].Value.ToString();

        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                string masp = dataGridView1.Rows[i].Cells[0].Value.ToString();
                string soluongban = dataGridView1.Rows[i].Cells[2].Value.ToString();
                string dongiaban = dataGridView1.Rows[i].Cells[3].Value.ToString();
                string tongtien = dataGridView1.Rows[i].Cells[4].Value.ToString();
                string soluongconlai = dataGridView1.Rows[i].Cells[5].Value.ToString();

                CT_HDBAN ct = new CT_HDBAN();
                ct.MaHDB = Convert.ToInt32(txtMaHD.Text);
                ct.MaSP = Convert.ToInt32(masp);
                ct.SoLuongBan = Convert.ToInt32(soluongban);
                ct.DonGiaBan = Convert.ToInt32(dongiaban);
                ct.TongTien = Convert.ToDouble(tongtien);
                BanHangDal.LuuChiTietBanHang(ct);

                BanHangDal.UpdateSanPham(Convert.ToInt32(masp), Convert.ToInt32(soluongconlai));
            }
            HOADONBAN hd = new HOADONBAN();
            hd.ThanhTienHDB = Convert.ToInt32(txtThanhTien.Text);
            BanHangDal.UpdateThanhTienHD(Convert.ToInt32(txtMaHD.Text), Convert.ToInt32(txtThanhTien.Text));
            MessageBox.Show("Bán hàng thành công! Vui lòng in hóa đơn", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            btnHuyHoaDon.Enabled = false;
            btnInHD.Enabled = true;
        }

        private void txtSoLuongMua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void btnHuyHoaDon_Click(object sender, EventArgs e)
        {
            try
            {
                HOADONNHAP hdn = new HOADONNHAP();
                hdn.MaHDN = Convert.ToInt32(txtMaHD.Text);
                //int makho = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                if (BanHangDal.XoaHoaDonBan(Convert.ToInt32(txtMaHD.Text)) == 1)
                {
                    MessageBox.Show("Đã hủy hóa đơn thành công rồi!", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Hủy hóa đơn thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }

            }
            catch
            {
                MessageBox.Show("Bạn không thể hủy!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            txtMaHD.Text = "";
            cbKhachHang.Text = "";
            txtGhiChu.Text = "";
            txtGhiChu.Text = "";
            txtSoLuongTon.Text = "";
            txtDonGiaBan.Text = "";
            txtMoTa.Text = "";
            txtSoLuongMua.Text = "";
           
        }

        private void btnInHD_Click(object sender, EventArgs e)
        {
            MaHoaDon = Convert.ToInt32(txtMaHD.Text);
            frmHoaDonBanHang f = new frmHoaDonBanHang();
            ReportHoaDonBanHang rpt = new ReportHoaDonBanHang();
            
            f.Show();
        }

        private void btnThemKH_Click(object sender, EventArgs e)
        {
            frmKhachHang f = new frmKhachHang();
            f.ShowDialog();

            cbKhachHang.DataSource = BanHangDal.LoadCbKhachHang();
            cbKhachHang.DisplayMember = "TenKH";
            cbKhachHang.ValueMember = "MaKH";
        }
    }
}