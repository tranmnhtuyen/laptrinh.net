﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace PM_PhoneStore_V2
{
    public partial class frmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        QL_NguoiDung CauHinh = new QL_NguoiDung();
        public frmDangNhap()
        {
            InitializeComponent();
        }

        public static string TenDangNhap;
        public static string MatKhau;
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            TenDangNhap = txtTaiKhoan.Text;
            if (string.IsNullOrEmpty(txtTaiKhoan.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống tài khoản");
                this.txtTaiKhoan.Focus();
                return;
            }
            if (string.IsNullOrEmpty(this.txtMatKhau.Text))
            {
                MessageBox.Show("Không được bỏ trống mật khẩu");
                this.txtMatKhau.Focus();
                return;
            }

            int kq = CauHinh.Check_Config();
            if (kq == 0)
            {
                ProcessLogin();// Cấu hình phù hợp xử lý đăng nhập
            }
            if (kq == 1)
            {
                MessageBox.Show("Chuỗi cấu hình không tồn tại");// Xử lý cấu hình
                ProcessConfig();
            }
            if (kq == 2)
            {
                MessageBox.Show("Chuỗi cấu hình không phù hợp");// Xử lý cấu hình
                ProcessConfig();
            }
        }

        public void ProcessLogin()
        {
            int result;
            result = CauHinh.Check_User(txtTaiKhoan.Text, txtMatKhau.Text);
            // Wrong username or pass
            if (result == 100)
            {
                MessageBox.Show("Sai thông tin mật khẩu");
                return;
            }
            // Account had been disabled
            else if (result == 200)
            {
                MessageBox.Show("Tài khoản bị khóa");
                return;
            }
            // 300
            //frmMain frm = new frmMain();
            //this.Hide();
            //frm.ShowDialog();
            //this.Show();


            if (Program.frmTrangChu == null || Program.frmTrangChu.IsDisposed)
            {
                Program.frmTrangChu = new frmTrangChu();
            }
            this.Hide();
            //Program.frmTrangChu.Text = txtMatKhau.Text;
            Program.frmTrangChu.Show();
        }
        public void ProcessConfig()
        {
            frmCauHinh frm = new frmCauHinh();

            frm.ShowDialog();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmDangNhap_FormClosing(object sender, FormClosingEventArgs e)
        {
            //base.OnClosing(e);
            //if (MessageBox.Show("Bạn có chắc muốn đóng ??", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.No)
            //{
            //    e.Cancel = true;
            //}
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = true;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            frmDangKiTK f = new frmDangKiTK();
            f.ShowDialog();
        }
    }
}