﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;

namespace PM_PhoneStore_V2
{
    public partial class frmDoiMK : DevExpress.XtraEditors.XtraForm
    {
        DoiMatKhauDAL dk = new DoiMatKhauDAL();
        public frmDoiMK()
        {
            InitializeComponent();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            //Kiem tra rong
            if (string.IsNullOrEmpty(txtmatkhaucu.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống mật khẩu củ! ");
                this.txtmatkhaucu.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtmatkhaumoi.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ trống mật khẩu mới! ");
                this.txtmatkhaumoi.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtnhaplaimk.Text.Trim()))
            {
                MessageBox.Show("Không được bỏ nhập lại mật khẩu! ");
                this.txtnhaplaimk.Focus();
                return;
            }

            if (txtmatkhaumoi.Text != txtnhaplaimk.Text)
            {
                MessageBox.Show("Nhập lại mâtk khẩu mới không khớp!");
            }
            else if(txtmatkhaucu.Text == frmDangNhap.MatKhau)
                {
                     MessageBox.Show("Mật khẩu cũ không đúng!");
                }
                else
                {
                    QL_NGUOIDUNG ql = new QL_NGUOIDUNG();
                    ql.MatKhau = txtmatkhaumoi.Text;
                    if (dk.UpdateMatKhau(ttxtendangnhap.Text, txtmatkhaumoi.Text) == 1)
                    {
                        MessageBox.Show("Đổi mật khẩu thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        MessageBox.Show("Đổi mật khẩu thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                }
                
        }

        private void frmDoiMK_Load(object sender, EventArgs e)
        {
            ttxtendangnhap.Enabled = false;
            ttxtendangnhap.Text = frmDangNhap.TenDangNhap;

        }
    }
}