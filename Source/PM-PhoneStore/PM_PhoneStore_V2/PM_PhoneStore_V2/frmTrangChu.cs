﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DAL_BLL;
using DevExpress.XtraBars.Ribbon;

namespace PM_PhoneStore_V2
{
    public partial class frmTrangChu : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        phanquyen pq = new phanquyen();
        public frmTrangChu()
        {
            InitializeComponent();
        }
        private Form IsActive(Type ftype)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.GetType() == ftype)
                    return f;
            }
            return null;
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frmNguoiDung));
            if (form == null)
            {
                frmNguoiDung f = new frmNguoiDung();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
            frmDangNhap f = new frmDangNhap();
            f.ShowDialog();
        }

        private void frmTrangChu_Load(object sender, EventArgs e)
        {
            
            
            DataTable dt = pq.loadphanquyen(frmDangNhap.TenDangNhap);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                foreach (DataRow mh in dt.Rows)
                {
                    FindMenuPhanQuyen(mh.ItemArray[0].ToString(), mh.ItemArray[1].ToString(), this.ribbonControl1);
                }
            }

        }
        private void FindMenuPhanQuyen(string pScreenName, string pEnable, RibbonControl mnuItems)
        {
            foreach (RibbonPage item in mnuItems.Pages)
            {
                foreach (RibbonPageGroup item2 in item.Groups)
                {
                    foreach (BarButtonItemLink item3 in item2.ItemLinks)
                    {
                        if (item3.Item.Name.ToString().CompareTo(pScreenName.Trim()) == 0)
                        {
                            if (pEnable.Trim() == "True")
                            {
                                item3.Item.Enabled = true;
                                item3.Item.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                            }
                            else if (pEnable.Trim() == "False")
                            {
                                item3.Item.Enabled = false;
                            }
                        }
                    }
                }
            }
        }
        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(BanHang));
            if (form == null)
            {
                BanHang f = new BanHang();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void MHNhap_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(NhapHang));
            if (form == null)
            {
                NhapHang f = new NhapHang();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void NhaCC_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frnNhaCungCap));
            if (form == null)
            {
                frnNhaCungCap f = new frnNhaCungCap();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void KhachHang_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frmKhachHang));
            if (form == null)
            {
                frmKhachHang f = new frmKhachHang();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void MHNV_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frmNhanVien));
            if (form == null)
            {
                frmNhanVien f = new frmNhanVien();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frmDoiMK));
            if (form == null)
            {
                frmDoiMK f = new frmDoiMK();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void frmTrangChu_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void PhanQuyen_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frmPhanQuyen));
            if (form == null)
            {
                frmPhanQuyen f = new frmPhanQuyen();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void ThemNDNhom_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frmThemNDVaoNhom));
            if (form == null)
            {
                frmThemNDVaoNhom f = new frmThemNDVaoNhom();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }

        private void barButtonItem4_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(frmSanPham));
            if (form == null)
            {
                frmSanPham f = new frmSanPham();
                f.MdiParent = this;
                f.Show();
            }
            else
            {
                form.Activate();
            }
        }
    }
}