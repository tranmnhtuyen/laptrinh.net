﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;

namespace PM_PhoneStore_V2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static frmDangNhap DangNhap = null;
        public static frmTrangChu frmTrangChu = null;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //BonusSkins.Register();
          Application.Run(new frmThongKeDoanhThu());

            DangNhap = new frmDangNhap();
          //Application.Run(DangNhap);
        }
    }
}
