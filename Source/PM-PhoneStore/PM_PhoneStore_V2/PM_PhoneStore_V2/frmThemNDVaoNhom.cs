﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;

namespace PM_PhoneStore_V2
{
    public partial class frmThemNDVaoNhom : DevExpress.XtraEditors.XtraForm
    {
        ThemNDVaoNhomDAL Dal = new ThemNDVaoNhomDAL();
        public frmThemNDVaoNhom()
        {
            InitializeComponent();
        }

        private void frmThemNDVaoNhom_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Dal.LoadDataGridNguoiDung();
            dataGridView2.DataSource = Dal.LoadDataGridNguoiDungNhomNguoiDung();

            cbNhomNguoiDung.DataSource = Dal.LoadCbNhomNguoiDung();
            cbNhomNguoiDung.DisplayMember = "TenNhom";
            cbNhomNguoiDung.ValueMember = "MaNhom";

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            //Kiểm tra rỗng
            if (string.IsNullOrEmpty(txtTenDangNhap.Text.Trim()))
            {
                MessageBox.Show("Vui lòng chọn nhân viên");
                this.txtTenDangNhap.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtMatKhau.Text.Trim()))
            {
                MessageBox.Show("Vui lòng chọn nhân viên");
                this.txtMatKhau.Focus();
                return;
            }

            QL_NguoiDungNhomNguoiDung nnd = new QL_NguoiDungNhomNguoiDung();
            nnd.TenDangNhap = txtTenDangNhap.Text;
            nnd.MaNhomNguoiDung = cbNhomNguoiDung.SelectedValue.ToString();
            if (Dal.ThemNguoiDungVaoNhom(nnd) == 1)
            {
                MessageBox.Show("Thêm thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("Thêm thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            dataGridView2.DataSource = Dal.LoadDataGridNguoiDungNhomNguoiDung();
            txtTenDangNhap.Enabled = true;
            txtMatKhau.Enabled = true;
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cbNhomNguoiDung.Enabled = false;
            int numrow;
            numrow = e.RowIndex;
            cbNhomNguoiDung.Text = dataGridView2.Rows[numrow].Cells["Column2"].Value.ToString();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            cbNhomNguoiDung.Enabled = true;
            try
            {
                QL_NguoiDungNhomNguoiDung ql = new QL_NguoiDungNhomNguoiDung();
                ql.MaNhomNguoiDung =cbNhomNguoiDung.Text;
                if (Dal.XoaNDKhoiNhom(ql.MaNhomNguoiDung) == 1)
                {
                    MessageBox.Show("Đã xóa thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Xóa thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                dataGridView2.DataSource = Dal.LoadDataGridNguoiDungNhomNguoiDung();
            }
            catch
            {
                MessageBox.Show("Bạn không thể xóa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            txtTenDangNhap.Enabled = true;
            txtMatKhau.Enabled = true;
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int numrow;
            numrow = e.RowIndex;
            txtTenDangNhap.Text = dataGridView1.Rows[numrow].Cells["TenDangNhap"].Value.ToString();
            txtMatKhau.Text = dataGridView1.Rows[numrow].Cells["MatKhau"].Value.ToString();
            txtTenDangNhap.Enabled = false;
            txtMatKhau.Enabled = false;
        }
    }
}