﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace PM_PhoneStore_V2
{
    public partial class frmHoaDonNhapHang : DevExpress.XtraEditors.XtraForm
    {
        public frmHoaDonNhapHang()
        {
            InitializeComponent();
        }

        private void frmHoaDonNhapHang_Load(object sender, EventArgs e)
        {

            ReportNhapHang rpt = new ReportNhapHang();
            crystalReportViewer1.ReportSource = rpt;
            crystalReportViewer1.Refresh();
            rpt.SetParameterValue("HoaDon", NhapHang.MaHoaDonNhap);
            rpt.SetDatabaseLogon("sa", "sa2012", "ADMIN\\SQLEXPRESS", "DB_MobileStore_v2");
        }
    }
}