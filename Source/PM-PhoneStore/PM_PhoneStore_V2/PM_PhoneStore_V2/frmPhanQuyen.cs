﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DAL_BLL;

namespace PM_PhoneStore_V2
{
    public partial class frmPhanQuyen : DevExpress.XtraEditors.XtraForm
    {
        PhanQuyenNhanVien Dal = new PhanQuyenNhanVien();
        public frmPhanQuyen()
        {
            InitializeComponent();
        }

        private void frmPhanQuyen_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Dal.LoadDataGridDMManHinh();
            dataGridView2.DataSource = Dal.LoadDataGridPhanQuyen();
            cbNhomNguoiDung.DataSource = Dal.LoadCbNhomNguoiDung();
            cbNhomNguoiDung.DisplayMember = "TenNhom";
            cbNhomNguoiDung.ValueMember = "MaNhom";
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int numrow;
            numrow = e.RowIndex;
            txtMaManHinh.Text = dataGridView1.Rows[numrow].Cells[0].Value.ToString();
            txtTenManHinh.Text = dataGridView1.Rows[numrow].Cells[1].Value.ToString();
        }

        private void cbNhomNguoiDung_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbNhomNguoiDung_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView2.DataSource = Dal.LoadDataGridPhanQuyen1(cbNhomNguoiDung.SelectedValue.ToString());
            cbNhomNguoiDung.Text = "";
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            ////Kiểm tra rỗng
            //if (string.IsNullOrEmpty(txtTenNCC.Text.Trim()))
            //{
            //    MessageBox.Show("Không được bỏ trống tên nhà cung cấp");
            //    this.txtTenNCC.Focus();
            //    return;
            //}

            PhanQuyen pq = new PhanQuyen();
            pq.MaNhomNguoiDung = cbNhomNguoiDung.SelectedValue.ToString();
            pq.MaManHinh = txtMaManHinh.Text;
            pq.CoQuyen = comboBox1.Text;
            if (Dal.ThemPhanQuyen(pq) == 1)
            {
                MessageBox.Show("Thêm thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                MessageBox.Show("Thêm thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            dataGridView2.DataSource = Dal.LoadDataGridPhanQuyen1(cbNhomNguoiDung.SelectedValue.ToString());
            txtMaManHinh.Text = "";
            txtTenManHinh.Text = "";
            cbNhomNguoiDung.Text = "";
            comboBox1.Text = "";
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int numrow1;
            numrow1 = e.RowIndex;
            cbNhomNguoiDung.Text = dataGridView2.Rows[numrow1].Cells["Column3"].Value.ToString();           
            //cbNhomNguoiDung.Text = dataGridView2.Rows[numrow].Cells[0].Value.ToString();
            txtMaManHinh.Text = dataGridView2.Rows[numrow1].Cells["Column4"].Value.ToString();
            comboBox1.Text = dataGridView2.Rows[numrow1].Cells["CoQuyen"].Value.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //PhanQuyen pq = new PhanQuyen();
            //pq.MaNhomNguoiDung = cbNhomNguoiDung.SelectedValue.ToString();
            //pq.MaManHinh = txtMaManHinh.Text;
            //pq.CoQuyen = comboBox1.Text;
            //if (Dal.UpdateQuyen(pq.MaNhomNguoiDung, pq.MaManHinh, pq.CoQuyen) == 1)
            //{
            //    MessageBox.Show("Đã sửa thành công rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            //}
            //else
            //{
            //    MessageBox.Show("Sửa thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            //}
            //dataGridView2.DataSource = Dal.LoadDataGridPhanQuyen1(cbNhomNguoiDung.SelectedValue.ToString());
            try
            {
                PhanQuyen pq = new PhanQuyen();
                pq.MaNhomNguoiDung = cbNhomNguoiDung.SelectedValue.ToString();
                pq.MaManHinh = txtMaManHinh.Text;
                if (Dal.XoaQuyen(pq.MaNhomNguoiDung, pq.MaManHinh) == 1)
                {
                    MessageBox.Show("Đã xóa thành công rồi!", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Xóa thất bại! Chúc bạn may mắn lần sau.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                dataGridView2.DataSource = Dal.LoadDataGridPhanQuyen1(cbNhomNguoiDung.SelectedValue.ToString());
            }
            catch
            {
                MessageBox.Show("Bạn không thể xóa!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            txtMaManHinh.Text = "";
            txtTenManHinh.Text = "";
            cbNhomNguoiDung.Text = "";
            comboBox1.Text = "";
        }
    }
}