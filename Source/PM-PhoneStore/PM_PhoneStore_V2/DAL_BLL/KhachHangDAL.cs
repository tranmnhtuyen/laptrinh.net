﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class KhachHangDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
        public KhachHangDAL()
        {
        }

        //Load DataGridView Khách Hàng
        public List<KHACHHANG> LoadDataGridKH()
        {
            return QLCH.KHACHHANGs.Select(t => t).ToList<KHACHHANG>();
        }

        //Luu thông tin Khách Hàng
        public int LuuThongTinKhachHang(KHACHHANG kh)
        {
            try
            {
                QLCH.KHACHHANGs.InsertOnSubmit(kh);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        //Sửa Thông tin Khách Hàng
        public int SuaThongTinKhachHang(int makh, string tenkh, string diachi, string sodienthoai)
        {
            try
            {
                KHACHHANG kh = QLCH.KHACHHANGs.Where(t => t.MaKH == makh).Single();
                kh.TenKH = tenkh;
                kh.DiaChi = diachi;
                kh.SdtKH = sodienthoai;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        //Xoa thong tin Khách Hàng
        public int XoaThongTinKhachHang(int makh)
        {
            KHACHHANG kh = QLCH.KHACHHANGs.Where(t => t.MaKH == makh).FirstOrDefault();
            try
            {
                QLCH.KHACHHANGs.DeleteOnSubmit(kh);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
