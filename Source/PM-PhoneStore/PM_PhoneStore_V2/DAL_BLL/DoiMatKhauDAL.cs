﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class DoiMatKhauDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
        public DoiMatKhauDAL() { }

        public int UpdateMatKhau(string tendangnhap, string matkhau)
        {

            try
            {
                QL_NGUOIDUNG nd = QLCH.QL_NGUOIDUNGs.Where(t => t.TenDangNhap == tendangnhap).Single();
                nd.MatKhau = matkhau;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
