﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    
    public class NhaCungCapDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
        public NhaCungCapDAL()
        {}
        //load data grid view nha cung cap
        public List<NHACUNGCAP> LoadDataGridNhaCC()
        {
            return QLCH.NHACUNGCAPs.Select(t => t).ToList<NHACUNGCAP>();
        }
        //Luu thông tin nhà cung cấp
        public int LuuTTNhaCC(NHACUNGCAP ncc)
        {
            try
            {
                QLCH.NHACUNGCAPs.InsertOnSubmit(ncc);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }

        }
        //xóa thông tin nhà cung cấp
        public int XoaNhaCC(int mancc)
        {
            NHACUNGCAP ncc = QLCH.NHACUNGCAPs.Where(t => t.MaNCC == mancc).FirstOrDefault();
            try
            {
                QLCH.NHACUNGCAPs.DeleteOnSubmit(ncc);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        //Sua thông tin nhà cung cấp
        public int SuaNhaCC(int mancc, string tenncc, string email, string sdt, string diachi)
        {

            try
            {
                NHACUNGCAP ncc = QLCH.NHACUNGCAPs.Where(t => t.MaNCC == mancc).Single();
                ncc.MaNCC = mancc;
                ncc.TenNCC = tenncc;
                ncc.Email = email;
                ncc.SdtNCC = sdt;
                ncc.DiaChi = diachi;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
