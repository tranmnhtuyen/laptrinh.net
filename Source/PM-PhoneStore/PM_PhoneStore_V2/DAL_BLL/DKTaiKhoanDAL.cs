﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
  
    public class DKTaiKhoanDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
        public DKTaiKhoanDAL() { }
        public int LuuThongTinTaiKhoan(QL_NGUOIDUNG nd)
        {
            try
            {
                QLCH.QL_NGUOIDUNGs.InsertOnSubmit(nd);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
