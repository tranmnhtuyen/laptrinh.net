﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class ThemNDVaoNhomDAL
    {
        public ThemNDVaoNhomDAL()
        { }

        QLCHDataContext QLCH = new QLCHDataContext();

        //Load DataGridView người dùng
        public List<QL_NGUOIDUNG> LoadDataGridNguoiDung()
        {
            return QLCH.QL_NGUOIDUNGs.Select(t => t).ToList<QL_NGUOIDUNG>();
        }

        //Load DataGridView người dùng nhom nguoi dung
        public List<QL_NguoiDungNhomNguoiDung> LoadDataGridNguoiDungNhomNguoiDung()
        {
            return QLCH.QL_NguoiDungNhomNguoiDungs.Select(t => t).ToList<QL_NguoiDungNhomNguoiDung>();
        }
        //load conbo nhom ngoi dung 
        public List<QL_NhomNguoiDung> LoadCbNhomNguoiDung()
        {
            return QLCH.QL_NhomNguoiDungs.Select(t => t).ToList<QL_NhomNguoiDung>();
        }
        // thêm nguoi dung vao nhóm 
        public int ThemNguoiDungVaoNhom(QL_NguoiDungNhomNguoiDung ql)
        {
            try
            {
                QLCH.QL_NguoiDungNhomNguoiDungs.InsertOnSubmit(ql);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        // Xóa nguoi dung ra khỏi nhóm
        public int XoaNDKhoiNhom(string manhom)
        {
            QL_NguoiDungNhomNguoiDung nv = QLCH.QL_NguoiDungNhomNguoiDungs.Where(t => t.MaNhomNguoiDung == manhom).FirstOrDefault();
            try
            {
                QLCH.QL_NguoiDungNhomNguoiDungs.DeleteOnSubmit(nv);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

    }
}
