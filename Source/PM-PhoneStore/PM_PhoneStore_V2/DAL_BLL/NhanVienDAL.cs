﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class NhanVienDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
        public NhanVienDAL()
        { }

        //Load DataGridView Nhân Viên
        public List<NHANVIEN> LoadDataGridNhanVien()
        {
            return QLCH.NHANVIENs.Select(t => t).ToList<NHANVIEN>();
        }

        //Luu thông tin nhân viên
        public int LuuThongTinNhanVien(NHANVIEN nv)
        {
            try
            {
                QLCH.NHANVIENs.InsertOnSubmit(nv);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        //Sửa Thông tin Nhân viên
        public int SuaThongTinNhanVien(int manv, string tennv,string tendangnhap, string diachi, string ngaysinh, string sodienthoai)
        {
            try
            {
                NHANVIEN nv = QLCH.NHANVIENs.Where(t => t.MaNV == manv).Single();
                nv.TenNV = tennv;
                nv.TenDangNhap = tendangnhap;
                nv.DiaChiNV = diachi;
                nv.NgaySinh = ngaysinh;
                nv.SdtNV = sodienthoai;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        //Xoa thong tin nhan vien
        public int XoaThongTinNhanVien(int manv)
        {
            NHANVIEN nv = QLCH.NHANVIENs.Where(t => t.MaNV == manv).FirstOrDefault();
            try
            {
                QLCH.NHANVIENs.DeleteOnSubmit(nv);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
