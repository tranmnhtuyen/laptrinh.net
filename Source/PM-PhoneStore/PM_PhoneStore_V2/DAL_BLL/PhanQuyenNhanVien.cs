﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class PhanQuyenNhanVien
    {
        QLCHDataContext QLCH = new QLCHDataContext();
         public PhanQuyenNhanVien() { }
        //Load DataGridView Man Hinh
        public List<DM_ManHinh> LoadDataGridDMManHinh()
        {
            return QLCH.DM_ManHinhs.Select(t => t).ToList<DM_ManHinh>();
        }
        //Load DataGridView Phan quyen
        public List<PhanQuyen> LoadDataGridPhanQuyen()
        {
            return QLCH.PhanQuyens.Select(t => t).ToList<PhanQuyen>();
        }
        //Load ComboBox San Pham
        public List<QL_NhomNguoiDung> LoadCbNhomNguoiDung()
        {
            return QLCH.QL_NhomNguoiDungs.Select(t => t).ToList<QL_NhomNguoiDung>();
        }
        //LoadDataGridPhanQuyen1
        public List<PhanQuyen> LoadDataGridPhanQuyen1(string manhom)
        {
            return QLCH.PhanQuyens.Where(t => t.MaNhomNguoiDung == manhom).Select(t => t).ToList<PhanQuyen>();
        }

        public int ThemPhanQuyen(PhanQuyen ph)
        {
            try
            {
                QLCH.PhanQuyens.InsertOnSubmit(ph);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        public int UpdateQuyen(string mannd,string mamh, string quyen)
        {
            try
            {
                PhanQuyen pq = QLCH.PhanQuyens.Where(t => t.MaNhomNguoiDung == mannd).Single();
                pq.CoQuyen = quyen;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public int XoaQuyen(string manhom, string mamh)
        {
            PhanQuyen kh = QLCH.PhanQuyens.Where(t => t.MaNhomNguoiDung == manhom ).FirstOrDefault();
            try
            {
                QLCH.PhanQuyens.DeleteOnSubmit(kh);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
