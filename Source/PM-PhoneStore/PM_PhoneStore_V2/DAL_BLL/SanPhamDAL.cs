﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class SanPhamDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
        public SanPhamDAL()
        { }
        //Load combobox Thuong Hiệu
        public List<THUONGHIEU> LoadCbnThuongHieu()
        {
            return QLCH.THUONGHIEUs.Select(t => t).ToList<THUONGHIEU>();
        }

        //Load DataGridView San pham
        public List<SANPHAM> LoadDataGridSanPham()
        {
            return QLCH.SANPHAMs.Select(t => t).ToList<SANPHAM>();
        }

        //Luu thong tin san pham
        public int LuuThongTinSanPham(SANPHAM sp)
        {
            try
            {
                QLCH.SANPHAMs.InsertOnSubmit(sp);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        //Xoa thong tin san pham
        public int XoaThongTinSanPhamn(int masp)
        {
            SANPHAM sp = QLCH.SANPHAMs.Where(t => t.MaSP == masp).FirstOrDefault();
            try
            {
                QLCH.SANPHAMs.DeleteOnSubmit(sp);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        //Sua thong tin san pham
        public int SuaThongTinSanPham(int masp, string tensp, int dongianhap, int dongiaban, int soluongton, string mota, int mathuonghieu)
        {
            try
            {
                SANPHAM sp = QLCH.SANPHAMs.Where(t => t.MaSP == masp).Single();
                sp.TenSP = tensp;
                sp.DonGiaNhap = dongianhap;
                sp.DonGiaBan = dongiaban;
                sp.SoLuongTon = soluongton;
                sp.MoTa = mota;
                sp.MaThuongHieu = mathuonghieu;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
