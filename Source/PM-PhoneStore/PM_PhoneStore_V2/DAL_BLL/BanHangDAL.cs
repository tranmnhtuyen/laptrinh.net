﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors;

namespace DAL_BLL
{
    
    public class BanHangDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
        public BanHangDAL()
        { }

        //Load ComboBox San Pham
        public List<SANPHAM> LoadCbSanPham()
        {
            return QLCH.SANPHAMs.Select(t => t).ToList<SANPHAM>();
        }

        //Load Thoong tin Khách Hàng
        public List<KHACHHANG> LoadCbKhachHang()
        {
            return QLCH.KHACHHANGs.Select(t => t).ToList<KHACHHANG>();
        }

        //Load Thoong tin Nhân viên
        public List<NHANVIEN> LoadCbNhanVien()
        {
            return QLCH.NHANVIENs.Select(t => t).ToList<NHANVIEN>();
        }

        //Luu Thong Tin Hóa Đơn bán
        public int LuuThongTinHoaDonBan(HOADONBAN hdb)
        {
            try
            {
                QLCH.HOADONBANs.InsertOnSubmit(hdb);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        // Lay ma Hoa Don 
        public string LayMaHoaDon()
        {
            int max1 = 0;
            int max2 = 0;
            var ma1 = from p in QLCH.HOADONBANs
                      select new
                      {
                          p.MaHDB
                      };

            foreach (var i in ma1)
            {
                if (max2 < i.MaHDB)
                {
                    max2 = i.MaHDB;
                }
            }
            if (max1 == max2 || max1 > max2)
                return (max1).ToString();
            return (max2).ToString();
        }

        //Lay thong tin san pham do len cac textbox
        public void LayThongTinSanPham(String masp, TextEdit dongiaban, TextEdit mota, TextEdit soluongton)
        {
            var ban = (from p in QLCH.SANPHAMs
                       select new
                       {
                           p.MaSP,
                           //p.TenSP,
                           //p.DonGiaNhap,
                           p.DonGiaBan,
                           p.MoTa,
                           //p.MaThuongHieu,
                           p.SoLuongTon
                       }).ToList();
            foreach (var p in ban)
            {
                if (p.MaSP.ToString().Trim() == masp.ToString())
                {
                    //tensp.Text = p.TenSP.Trim();
                    //dongianhap.Text = p.DonGiaNhap.ToString().Trim();
                    dongiaban.Text = p.DonGiaBan.ToString().Trim();
                    mota.Text = p.MoTa.ToString().Trim();
                   // thuonghieu.Text = p.MaThuongHieu.ToString().Trim();
                    soluongton.Text = p.SoLuongTon.ToString().Trim();
                }
            }
        }
        //Luu Chi tiet don Hang
        public int LuuChiTietBanHang(CT_HDBAN ct)
        {
            try
            {
                QLCH.CT_HDBANs.InsertOnSubmit(ct);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        //Update San Pham 
        public int UpdateSanPham(int masp, int soluong)
        {

            try
            {
                SANPHAM sp = QLCH.SANPHAMs.Where(t => t.MaSP == masp).Single();
                sp.SoLuongTon = soluong;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        public string Soluongtonkho(int masp)
        {
            return QLCH.SANPHAMs.Where(t => t.MaSP == masp).Select(t =>t).ToString();
        }
        public int UpdateThanhTienHD(int mahdb, int thanhtien)
        {

            try
            {
                HOADONBAN hd = QLCH.HOADONBANs.Where(t => t.MaHDB == mahdb).Single();
                hd.ThanhTienHDB = thanhtien;
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
        public int XoaHoaDonBan(int mahdb)
        {
            HOADONBAN hdn = QLCH.HOADONBANs.Where(t => t.MaHDB == mahdb).FirstOrDefault();
            try
            {
                QLCH.HOADONBANs.DeleteOnSubmit(hdn);
                QLCH.SubmitChanges();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
