﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class phanquyen
    {
        QLCHDataContext qltg = new QLCHDataContext();
        public DataTable loadphanquyen(string tendn)
        {
            var mn = from kh in qltg.QL_NGUOIDUNGs
                     join manom in qltg.QL_NguoiDungNhomNguoiDungs on kh.TenDangNhap equals manom.TenDangNhap
                     where manom.TenDangNhap == tendn
                     select new
                     {
                         manom.MaNhomNguoiDung,
                     };
            var c = from manhom in mn
                    join mamh in qltg.PhanQuyens on manhom.MaNhomNguoiDung equals mamh.MaNhomNguoiDung
                    select new
                    {
                        mamh.MaManHinh,
                        mamh.CoQuyen
                    };
            DataTable dt = new DataTable();
            dt.Columns.Add("MaManHinh");
            dt.Columns.Add("CoQuyen");
            foreach (var r in c)
            {
                dt.Rows.Add(r.MaManHinh, r.CoQuyen);
            }
            return dt;
        }
    }
}
