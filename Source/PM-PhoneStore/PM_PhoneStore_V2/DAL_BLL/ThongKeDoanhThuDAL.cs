﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_BLL
{
    public class ThongKeDoanhThuDAL
    {
        QLCHDataContext QLCH = new QLCHDataContext();
         public ThongKeDoanhThuDAL() { }
        public List<HOADONBAN> LoadDataGridHoaDonBan()
        {
            return QLCH.HOADONBANs.Select(t => t).ToList<HOADONBAN>();
        }
        public List<NHANVIEN> LoadCbnNhanVien()
        {
            return QLCH.NHANVIENs.Select(t => t).ToList<NHANVIEN>();
        }
        public List<HOADONBAN> LoadDataGridHoaDonBanTheoNV(int manv)
        {
            return QLCH.HOADONBANs.Where(t => t.MaNV == manv).Select(t => t).ToList<HOADONBAN>();
        }

    }
}
